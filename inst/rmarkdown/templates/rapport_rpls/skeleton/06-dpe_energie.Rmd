# Le parc social avec un DPE énergie A,B ou C {#dpe_energie}

```{r map_dep_energie,fig.width=14,fig.height=20}
composeur_carte(indicateur="DPE énergie classe A,B ou C",
                titre="Part des logements sociaux avec un DPE énergie A,B ou C au 1er janvier 2018",
                soustitre="En %",
                parc_recent=T,
                basdepage=NULL,
                filtre_zero=F,
                na_recode="Pas de QPV")

```


```{r comparaison_parc_recent_dpe_energie,fig.width=14,fig.height=8}
p_graphique_comparaison_parc_recent_dpe_energie<-graphique_comparaison_parc_recent(indicateur="DPE énergie classe A,B ou C",title="Logements sociaux avec un DPE énergie de classe A,B ou C",caption=caption)
p_graphique_comparaison_parc_recent_dpe_energie
```

```{r fig.width=12,fig.height = 6}
graph_repartition_par_date(indicateur=dpeenergie_red,
                           titre="Répartition des logements par DPE énergie",
                           )
```
